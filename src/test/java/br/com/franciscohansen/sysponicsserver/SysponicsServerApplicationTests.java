package br.com.franciscohansen.sysponicsserver;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;
import java.util.logging.Logger;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class SysponicsServerApplicationTests {
    Logger LOG = Logger.getLogger(SysponicsServerApplication.class.getSimpleName());

    @Test
    public void contextLoads() {
        LOG.info(RandomStringUtils.randomAlphanumeric(16).toLowerCase());
        String uuid = UUID.randomUUID().toString();
        LOG.info(uuid);
        LOG.info(uuid.replace("-", "").substring(0, 16));
    }

}
