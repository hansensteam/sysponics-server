package br.com.franciscohansen.sysponicsserver.util;

import java.text.SimpleDateFormat;
import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.TimeZone;

public class SchedulerUtils {
  public static LocalTime localTimeOfTime( Date date, boolean isStart ) {
    int hour, min, sec, nano;
    if ( date != null ) {
      SimpleDateFormat sdf = new SimpleDateFormat( "HH:mm:ss.SSSSSSSSS" );
      String time = sdf.format( date );
      LocalTime executionTime = LocalTime.parse( time, DateTimeFormatter.ofPattern( "HH:mm:ss.SSSSSSSSS" ) );
      hour = executionTime.getHour();
      min = executionTime.getMinute();
      sec = executionTime.getSecond();
      nano = executionTime.getNano();
    }
    else if ( isStart ) {
      hour = 0;
      min = 0;
      sec = 0;
      nano = 0;
    }
    else {
      hour = 23;
      min = 59;
      sec = 59;
      nano = 999999999;
    }
    return LocalTime.of( hour, min, sec, nano );
  }

  public static long secondsBetween( Date start, Date end ) {
    ZoneId zoneId = TimeZone.getDefault().toZoneId();

    LocalTime lStart = localTimeOfTime( start, true );
    LocalTime lEnd = localTimeOfTime( end, false );
    if ( lEnd.isBefore( lStart ) ) {
      ZonedDateTime dStart = ZonedDateTime.of( LocalDate.now(), lStart, zoneId );
      ZonedDateTime dEnd = ZonedDateTime.of( LocalDate.now(), lEnd, zoneId );
      dEnd = dEnd.plusDays( 1 );
      Duration duration = Duration.between( dStart, dEnd );
      return duration.getSeconds();
    }
    else {
      return Duration.between( lStart, lEnd ).getSeconds();
    }
  }

  public static long computeInitialDelay( Date timeToStart, Date timeToEnd ) {
    ZoneId zoneId = TimeZone.getDefault().toZoneId();

    LocalDateTime now = LocalDateTime.now();
    LocalTime start = localTimeOfTime( timeToStart, true );
    LocalTime end = localTimeOfTime( timeToEnd, false );
    LocalDateTime dStart = LocalDateTime.of( LocalDate.now(), start );
    LocalDateTime dEnd = LocalDateTime.of( LocalDate.now(), end );
    if( dEnd.isBefore( dStart )){
      dEnd = dEnd.plusDays( 1 );
    }

    if ( now.isAfter( dStart ) && now.isBefore( dEnd ) ) { //Se está entre, retorna 0, pra iniciar já
      return 0L;
    }
    else if ( now.isBefore( dStart ) ) { //Se antes do inicio, retorna a diferença
      Duration duration = Duration.between( now, dStart );
      return duration.getSeconds();
    }
    else { //Se depois do Fim, adiciona 1 dia e calcula.
      ZonedDateTime dNow = ZonedDateTime.of( LocalDate.now(), LocalTime.now(), zoneId );
      ZonedDateTime zdStart = ZonedDateTime.of( LocalDate.now(), start, zoneId );
      zdStart = zdStart.plusDays( 1 );
      Duration duration = Duration.between( dNow, zdStart );
      return duration.getSeconds();
    }
  }
}
