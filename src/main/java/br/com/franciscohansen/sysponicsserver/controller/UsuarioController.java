package br.com.franciscohansen.sysponicsserver.controller;

import br.com.franciscohansen.sysponicsserver.model.Usuario;
import br.com.franciscohansen.sysponicsserver.persistence.repository.UsuarioRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UsuarioController extends AbstractController<Usuario, UsuarioRepository, UsuarioService> {

    private UsuarioRepository repository;
    private UsuarioService service;
    private PasswordEncoder encoder;

    @Autowired
    public UsuarioController(UsuarioRepository repository, UsuarioService service, PasswordEncoder encoder) {
        this.repository = repository;
        this.service = service;
        this.encoder = encoder;
    }

    @Override
    protected UsuarioRepository getRepository() {
        return this.repository;
    }

    @Override
    protected UsuarioService getService() {
        return this.service;
    }


    @PostMapping("/register")
    public Usuario doRegister(@RequestBody Usuario usuario) {
        usuario = this.service.register(usuario);
        return usuario;
    }

    @GetMapping("/user-info")
    public Principal getUserInfo(Principal principal) {
        return principal;
    }

    @PostMapping("/change-password")
    public Usuario doChangePassword(Usuario usuario) {
        return null;
    }

    @Override
    protected Class<Usuario> getClassOfT() {
        return Usuario.class;
    }
}
