package br.com.franciscohansen.sysponicsserver.controller;

import br.com.franciscohansen.sysponicsserver.dto.CropLastDataDTO;
import br.com.franciscohansen.sysponicsserver.model.enums.EHistoricalDataType;
import br.com.franciscohansen.sysponicsserver.model.project.Crop;
import br.com.franciscohansen.sysponicsserver.model.project.HistoricalData;
import br.com.franciscohansen.sysponicsserver.persistence.repository.CropRepository;
import br.com.franciscohansen.sysponicsserver.persistence.repository.HistoricalDataRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.CropService;
import br.com.franciscohansen.sysponicsserver.persistence.specification.HistoricalDataSpecification;
import org.hibernate.annotations.ColumnTransformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping( "/crops" )
public class CropController extends AbstractController<Crop, CropRepository, CropService> {

  private CropRepository repository;
  private CropService service;
  private HistoricalDataRepository hdRepository;

  @Autowired
  public CropController( CropRepository repository, CropService service, HistoricalDataRepository hdRepository ) {
    this.repository = repository;
    this.service = service;
    this.hdRepository = hdRepository;
  }

  @Override
  protected CropRepository getRepository() {
    return repository;
  }

  @Override
  protected CropService getService() {
    return service;
  }

  @GetMapping( "{project_key}/active" )
  public Crop getActiveCrop( @PathVariable( "project_key" ) String projectKey ) {
    List<Crop> list = getRepository().findAllByProjectKey( projectKey, Sort.by( "plantingDate" ).descending() )
            .stream()
            .filter( crop -> !Optional.ofNullable( crop.getHarvested() ).orElse( false ) )
            .collect( Collectors.toList() );
    return list.isEmpty() ? null : list.get( 0 );
  }

  @GetMapping( "/{project_key}/all" )
  public List<Crop> getProjectCrops( @PathVariable( "project_key" ) String projectKey ) {
    return getRepository().findAllByProjectKey( projectKey, Sort.by( "plantingDate" ).descending() );
  }

  @GetMapping( "/last-data/{crop-id}" )
  public CropLastDataDTO getLastData( @PathVariable( "crop-id" ) Long cropId ) {
    CropLastDataDTO.CropLastDataDTOBuilder builder = CropLastDataDTO.builder();
    if ( cropId > 0 ) {
      for ( EHistoricalDataType type : EHistoricalDataType.values() ) {
        List<HistoricalData> list = hdRepository.findAll( Specification.where( HistoricalDataSpecification.whereCropIdAndTypeEquals( cropId, type ) ), Sort.by( "timestamp" ).descending() );
        if ( !list.isEmpty() ) {
          HistoricalData hd = list.get( 0 );
          switch ( hd.getType() ) {
            case WATER_TEMP:
              builder.waterTemp( Optional.ofNullable( hd.getValue() ).orElse( 0D ) );
              break;
            case WATER_PH:
              builder.waterPh( Optional.ofNullable( hd.getValue() ).orElse( 0D ) );
              break;
            case AIR_TEMP:
              builder.airTemp( Optional.ofNullable( hd.getValue() ).orElse( 0D ) );
              break;
            case AIR_HUM:
              builder.airHum( Optional.ofNullable( hd.getValue() ).orElse( 0D ) );
              break;
            case WATER_LEVEL:
              builder.waterLevelOk( Optional.ofNullable( hd.getValue() ).orElse( 0D ) == 0 );
              break;
            case LIGHTS:
              builder.lightsOn( Optional.ofNullable( hd.getValue() ).orElse( 0D ) == 1 );
              break;
            case WATER_PUMP:
              builder.pumpOn( Optional.ofNullable( hd.getValue() ).orElse( 0D ) == 1 );
              break;
          }
        }
      }
    }
    return builder.build();
  }

  @PostMapping( "/mark-harvested" )
  @Transactional
  public Boolean markCropAsHarvested( @RequestParam( "crop-id" ) Long cropId,
                                      @RequestParam( "species-id" ) Long speciesId,
                                      @RequestParam( "harvest-date" )
                                      @DateTimeFormat( pattern = "dd/MM/yyyy" ) Date date,
                                      @RequestParam( "obs" ) String observacoes ) {
    Optional<Crop> opt = this.repository.findById( cropId );
    if ( opt.isPresent() ) {
      Crop crop = opt.get();
      crop.setHarvested( true );
      crop.setActualHarvestDate( date );
      crop.setObservacoes( observacoes );
      crop.setHarvestedSpeciesId( speciesId );
      this.repository.save( crop );
      return true;
    }
    else {
      return false;
    }
  }

  @Override
  protected Class<Crop> getClassOfT() {
    return Crop.class;
  }
}
