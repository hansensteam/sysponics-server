package br.com.franciscohansen.sysponicsserver.controller;

import br.com.franciscohansen.sysponicsserver.dto.SpeciesDTO;
import br.com.franciscohansen.sysponicsserver.model.Species;
import br.com.franciscohansen.sysponicsserver.model.SpeciesNames;
import br.com.franciscohansen.sysponicsserver.persistence.repository.SpeciesRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.SpeciesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping( "/species" )
public class SpeciesController extends AbstractController<Species, SpeciesRepository, SpeciesService> {

  private SpeciesRepository repository;
  private SpeciesService service;

  @Autowired
  public SpeciesController( SpeciesRepository repository, SpeciesService service ) {
    this.repository = repository;
    this.service = service;
  }

  @Override
  protected SpeciesRepository getRepository() {
    return repository;
  }

  @Override
  protected SpeciesService getService() {
    return service;
  }

  @Override
  protected Class<Species> getClassOfT() {
    return Species.class;
  }

  @GetMapping( value = "/of-locale", produces = MediaType.APPLICATION_JSON_UTF8_VALUE )
  @Transactional
  public List<SpeciesDTO> getSpeciesOfLocale( @RequestParam( "locale" ) String locale,
                                              @RequestParam( required = false ) Integer page,
                                              @RequestParam( required = false ) Integer size ) {
    List<Species> list = new ArrayList<>();
    if ( page != null ) {
      list = this.repository.findAll( PageRequest.of( page, size ) )
              .stream()
              .collect( Collectors.toList() );
    }
    else {
      list = this.repository.findAll();
    }
    List<SpeciesDTO> response = new ArrayList<>();
    for ( Species species : list ) {
      SpeciesDTO.SpeciesDTOBuilder builder = SpeciesDTO.builder()
              .id( species.getId() )
              .cientificName( species.getCientificName() );
      for ( SpeciesNames name : species.getCommonNames() ) {
        if ( name.getLocale().equalsIgnoreCase( locale ) ) {
          builder.commonName( name.getCommonName() );
          break;
        }
      }
      response.add( builder.build() );
    }
    return response;
  }

  @GetMapping( value = "/of-locale-by-id/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE )
  @Transactional
  public SpeciesDTO getSpeciesOfLocaleById( @PathVariable( "id" ) Long speciesId,
                                            @RequestParam( "locale" ) String locale ) {
    Optional<Species> opt = this.repository.findById( speciesId );
    if ( opt.isPresent() ) {
      Species species = opt.get();
      SpeciesDTO.SpeciesDTOBuilder builder = SpeciesDTO.builder()
              .id( species.getId() )
              .cientificName( species.getCientificName() );
      for ( SpeciesNames name : species.getCommonNames() ) {
        if ( name.getLocale().equalsIgnoreCase( locale ) ) {
          builder.commonName( name.getCommonName() );
          break;
        }
      }
      return builder.build();
    }
    else {
      return null;
    }
  }
}
