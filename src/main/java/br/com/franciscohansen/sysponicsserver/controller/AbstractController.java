package br.com.franciscohansen.sysponicsserver.controller;

import br.com.franciscohansen.sysponicsserver.model.AbstractModel;
import br.com.franciscohansen.sysponicsserver.persistence.repository.IRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.IService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.io.IOException;


public abstract class AbstractController<T extends AbstractModel, U extends IRepository<T>, V extends IService<T, U>> {


  @Autowired
  private ObjectMapper om;

  protected abstract U getRepository();

  protected abstract V getService();

  protected abstract Class<T> getClassOfT();


  @PostMapping( value = "/persist", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE )
  public T save( @NotNull @RequestBody String obj ) throws IOException {
//    ObjectMapper om = builder.build();
    T t = om.readValue( obj, getClassOfT() );
    return getService().save( t );
  }

  @DeleteMapping( value = "/remove/{id}" )
  @Transactional
  public boolean remove( @PathVariable long id ) {
    T obj = getRepository().getOne( id );
    obj.setDeleted( true );
    getRepository().save( obj );
    return true;
  }

  @GetMapping( "/by-id/{id}" )
  public T byId( @PathVariable long id ) {
    return getRepository().getOne( id );
  }


}
