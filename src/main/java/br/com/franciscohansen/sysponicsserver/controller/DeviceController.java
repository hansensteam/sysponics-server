package br.com.franciscohansen.sysponicsserver.controller;

import br.com.franciscohansen.sysponicsserver.model.Device;
import br.com.franciscohansen.sysponicsserver.persistence.repository.DeviceRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/devices")
public class DeviceController extends AbstractController<Device, DeviceRepository, DeviceService> {

    private DeviceService service;
    private DeviceRepository repository;

    @Autowired
    public DeviceController(DeviceService service, DeviceRepository repository) {
        this.service = service;
        this.repository = repository;
    }

    @Override
    protected DeviceRepository getRepository() {
        return repository;
    }

    @Override
    protected DeviceService getService() {
        return service;
    }

    @Override
    protected Class<Device> getClassOfT() {
        return Device.class;
    }

}
