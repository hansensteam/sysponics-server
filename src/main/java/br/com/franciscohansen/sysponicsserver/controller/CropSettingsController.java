package br.com.franciscohansen.sysponicsserver.controller;

import br.com.franciscohansen.sysponicsserver.model.project.CropSettings;
import br.com.franciscohansen.sysponicsserver.persistence.repository.CropSettingsRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.CropSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping( "/cropSettings" )
public class CropSettingsController extends AbstractController<CropSettings, CropSettingsRepository, CropSettingsService> {


  private CropSettingsRepository repository;
  private CropSettingsService service;

  @Autowired
  public CropSettingsController( CropSettingsRepository repository, CropSettingsService service ) {
    this.repository = repository;
    this.service = service;
  }


  @Override
  protected CropSettingsRepository getRepository() {
    return repository;
  }

  @Override
  protected CropSettingsService getService() {
    return service;
  }

  @Override
  protected Class<CropSettings> getClassOfT() {
    return CropSettings.class;
  }

  @GetMapping(value = "/find-by-crop-id/{id}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE )
  public CropSettings findByCropId( @PathVariable("id") Long cropId ){
    return getRepository().findFirstByCrop_Id( cropId );
  }
}
