package br.com.franciscohansen.sysponicsserver.controller;

import br.com.franciscohansen.sysponicsserver.model.project.Project;
import br.com.franciscohansen.sysponicsserver.persistence.repository.ProjectRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/projects")
public class ProjectController extends AbstractController<Project, ProjectRepository, ProjectService> {
    private ProjectRepository repository;
    private ProjectService service;

    @Autowired
    public ProjectController(ProjectRepository repository, ProjectService service) {
        this.repository = repository;
        this.service = service;
    }


    @Override
    protected ProjectRepository getRepository() {
        return repository;
    }

    @Override
    protected ProjectService getService() {
        return service;
    }

    @Override
    protected Class<Project> getClassOfT() {
        return Project.class;
    }
}
