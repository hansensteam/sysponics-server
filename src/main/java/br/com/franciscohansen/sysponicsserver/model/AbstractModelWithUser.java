package br.com.franciscohansen.sysponicsserver.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.EqualsAndHashCode;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.hibernate.annotations.Where;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
@Where( clause = " ( (deleted is null) or (deleted=false) )" )
public abstract class AbstractModelWithUser extends AbstractModel {

  @ManyToOne( optional = false )
  @JoinColumn( name = "user_id", referencedColumnName = "id" )
  private Usuario usuario;

  public AbstractModelWithUser( String key, Usuario usuario ) {
    super( key );
    this.usuario = usuario;
  }

  public AbstractModelWithUser() {
  }

  public Usuario getUsuario() {
    return usuario;
  }

  public void setUsuario( Usuario usuario ) {
    this.usuario = usuario;
  }
}
