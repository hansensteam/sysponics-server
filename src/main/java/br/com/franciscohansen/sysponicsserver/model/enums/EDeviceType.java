package br.com.franciscohansen.sysponicsserver.model.enums;

import static br.com.franciscohansen.sysponicsserver.model.enums.EHistoricalDataType.*;

public enum EDeviceType {

  WATER_TEMP_SENSOR,
  WATER_PH_SENSOR,
  WATER_CE_SENSOR,
  AIR_TEMP_SENSOR,
  AIR_HUM_SENSOR,
  AIR_TEMP_HUM_SENSOR,
  ACT_LIGHTS,
  ACT_WATER_PUMP,
  ACT_AIR_PUMP,
  ACT_FAN,
  ACT_WATER_CHILLER,
  ACT_AIR_CHILLER,
  ACT_WATER_HEATER,
  ACT_AIR_HEATER,
  ACT_SPRINKLER,
  WATER_LEVEL_SENSOR;

  public EScheduleType getScheduleType() {
    switch ( this ) {
      case ACT_WATER_PUMP:
        return EScheduleType.WATER_PUMP;
      case ACT_WATER_CHILLER:
      case ACT_WATER_HEATER:
        return EScheduleType.WATER_TEMP;
      case ACT_AIR_CHILLER:
      case ACT_AIR_HEATER:
        return EScheduleType.AIR_TEMP;
      case ACT_SPRINKLER:
        return EScheduleType.AIR_HUM;
      default:
        return EScheduleType.LIGHTS;
    }
  }

  public EHistoricalDataType getHDType() {
    switch ( this ) {

      case WATER_TEMP_SENSOR:
        return WATER_TEMP;
      case WATER_PH_SENSOR:
        return WATER_PH;
      case AIR_TEMP_SENSOR:
        return AIR_TEMP;
      case AIR_HUM_SENSOR:
        return AIR_HUM;
      case ACT_LIGHTS:
        return LIGHTS;
      case ACT_WATER_PUMP:
        return WATER_PUMP;
      default:
        return WATER_PUMP;
    }
  }


}
