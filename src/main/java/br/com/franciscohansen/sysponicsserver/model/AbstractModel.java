package br.com.franciscohansen.sysponicsserver.model;

import br.com.franciscohansen.sysponicsserver.persistence.listeners.AbstractEntityListener;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.io.Serializable;
import java.util.UUID;

@MappedSuperclass
@EntityListeners( AbstractEntityListener.class )
@Where( clause = " ( (deleted is null) or (deleted=false) )" )
public abstract class AbstractModel implements Serializable {

  @Id
  @GeneratedValue( strategy = GenerationType.IDENTITY )
  private Long id;

  @Column( unique = true )
  private String key;

  @Column( columnDefinition = "BOOLEAN DEFAULT FALSE" )
  private Boolean deleted;

  public AbstractModel( String key ) {
    this.key = key;
  }

  public AbstractModel() {
    this.key = UUID.randomUUID().toString()
            .replace( "-", "" )
            .substring( 0, 16 );
  }


  public Long getId() {
    return id;
  }

  public void setId( Long id ) {
    this.id = id;
  }

  public String getKey() {
    return key;
  }

  public void setKey( String key ) {
    this.key = key;
  }

  public Boolean getDeleted() {
    return deleted;
  }

  public void setDeleted( Boolean deleted ) {
    this.deleted = deleted;
  }
}
