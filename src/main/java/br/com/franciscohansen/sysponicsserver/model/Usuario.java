package br.com.franciscohansen.sysponicsserver.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.hibernate.annotations.Where;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Column;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Where( clause = " ( (deleted is null) or (deleted=false) )" )
public class Usuario extends AbstractModel implements UserDetails {

  @Column
  private String nome;
  @Column( unique = true )
  private String email;
  @Column( nullable = false )
  @JsonIgnore
  @JsonDeserialize
  private String password;

  @Override
  @JsonIgnore
  public String getPassword() {
    return password;
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    List<GrantedAuthority> gas = new ArrayList<>();
    gas.add( new GrantedAuthority() {
      @Override
      public String getAuthority() {
        return "ROLE_USER";
      }
    } );
    return gas;
  }

  @Override
  public String getUsername() {
    return this.email;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return true;
  }

  public String getNome() {
    return nome;
  }

  public void setNome( String nome ) {
    this.nome = nome;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail( String email ) {
    this.email = email;
  }

  public void setPassword( String password ) {
    this.password = password;
  }
}
