package br.com.franciscohansen.sysponicsserver.model.project;

import br.com.franciscohansen.sysponicsserver.model.AbstractModel;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Where( clause = " ( (deleted is null) or (deleted=false) )" )
public class CropSettings extends AbstractModel {

  @OneToOne
  @JoinColumn( name = "crop_id", referencedColumnName = "id" )
  @JsonIgnore
  @JsonDeserialize
  private Crop crop;

  private Double waterPH;

  @Column
  private Long millisToVerify;

  @OneToMany( orphanRemoval = true, cascade = CascadeType.ALL, mappedBy = "cropSettings" )
  private List<CropSchedules> schedules = new ArrayList<>();

  @Builder
  public CropSettings( String key, Crop crop, Double waterPH, List<CropSchedules> schedules, Long millisToVerify ) {
    super( key );
    this.crop = crop;
    this.waterPH = waterPH;
    this.schedules = schedules;
    this.millisToVerify = millisToVerify;
  }

  public CropSettings() {
    this.millisToVerify = 60000L;
  }

  public void addSchedule( CropSchedules sch ) {
    sch.setCropSettings( this );
    schedules.add( sch );
  }

  @JsonInclude
  @JsonProperty( "crop_id" )
  public Long getCropId() {
    return crop != null ? crop.getId() : 0L;
  }

  public Crop getCrop() {
    return crop;
  }

  public void setCrop( Crop crop ) {
    this.crop = crop;
  }

  public Double getWaterPH() {
    return waterPH;
  }

  public void setWaterPH( Double waterPH ) {
    this.waterPH = waterPH;
  }

  public Long getMillisToVerify() {
    return millisToVerify;
  }

  public void setMillisToVerify( Long millisToVerify ) {
    this.millisToVerify = millisToVerify;
  }

  public List<CropSchedules> getSchedules() {
    return schedules;
  }

  public void setSchedules( List<CropSchedules> schedules ) {
    this.schedules = schedules;
  }
}
