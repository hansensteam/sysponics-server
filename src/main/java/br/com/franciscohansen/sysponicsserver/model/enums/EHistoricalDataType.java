package br.com.franciscohansen.sysponicsserver.model.enums;

public enum EHistoricalDataType {
    WATER_TEMP,
    WATER_PH,
    AIR_TEMP,
    AIR_HUM,
    WATER_LEVEL,
    LIGHTS,
    WATER_PUMP
}
