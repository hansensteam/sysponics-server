package br.com.franciscohansen.sysponicsserver.model.enums;

public enum EScheduleType {

    LIGHTS,
    WATER_TEMP,
    AIR_TEMP,
    AIR_HUM,
    WATER_PUMP

}
