package br.com.franciscohansen.sysponicsserver.model;

import br.com.franciscohansen.sysponicsserver.model.enums.EDeviceType;
import br.com.franciscohansen.sysponicsserver.model.project.Project;
import br.com.franciscohansen.sysponicsserver.persistence.listeners.AbstractEntityListener;
import br.com.franciscohansen.sysponicsserver.persistence.repository.DeviceRepository;
import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.hibernate.annotations.Where;

import javax.persistence.*;

@Entity
@Where( clause = " ( (deleted is null) or (deleted=false) )" )
public class Device extends AbstractModelWithUser {

  @ManyToOne()
  @JoinColumn( name = "project_id", referencedColumnName = "id" )
  @JsonIgnore
  private Project project;

  private String name;
  private String description;
  private EDeviceType deviceType;

  @Builder
  public Device( String key, Usuario usuario, String name, String description, EDeviceType deviceType ) {
    super( key, usuario );
    this.name = name;
    this.description = description;
    this.deviceType = deviceType;
  }

  public Device() {
  }

  public Project getProject() {
    return project;
  }

  public void setProject( Project project ) {
    this.project = project;
  }

  public String getName() {
    return name;
  }

  public void setName( String name ) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription( String description ) {
    this.description = description;
  }

  public EDeviceType getDeviceType() {
    return deviceType;
  }

  public void setDeviceType( EDeviceType deviceType ) {
    this.deviceType = deviceType;
  }

}
