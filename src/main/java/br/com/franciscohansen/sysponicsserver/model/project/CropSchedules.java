package br.com.franciscohansen.sysponicsserver.model.project;

import br.com.franciscohansen.sysponicsserver.model.AbstractModel;
import br.com.franciscohansen.sysponicsserver.model.enums.EScheduleType;
import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Where( clause = " ( (deleted is null) or (deleted=false) )" )
public class CropSchedules extends AbstractModel {

  @ManyToOne( optional = false )
  @JoinColumn( name = "crop_settings_id", referencedColumnName = "id" )
  @JsonIgnore
  private CropSettings cropSettings;

  @Column
  @Temporal( TemporalType.TIME )
  @JsonFormat( pattern = "HH:mm:ss", timezone = "America/Sao_Paulo", locale = "pt_BR" )
  private Date startTime;
  @Column
  @Temporal( TemporalType.TIME )
  @JsonFormat( pattern = "HH:mm:ss", timezone = "America/Sao_Paulo", locale = "pt_BR" )
  private Date endTime;
  @Enumerated( EnumType.STRING )
  private EScheduleType scheduleType;
  private Boolean poweredOn;
  private Double value;

  @Builder
  public CropSchedules( String key, Date startTime, Date endTime, EScheduleType scheduleType, Boolean poweredOn, Double value ) {
    super( key );
    this.startTime = startTime;
    this.endTime = endTime;
    this.scheduleType = scheduleType;
    this.poweredOn = poweredOn;
    this.value = value;
  }

  public CropSchedules() {
  }

  @JsonInclude
  @JsonProperty( "settings_id" )
  public Long getSettingsId() {
    return cropSettings != null ? cropSettings.getId() : 0L;
  }


  public CropSettings getCropSettings() {
    return cropSettings;
  }

  public void setCropSettings( CropSettings cropSettings ) {
    this.cropSettings = cropSettings;
  }

  public Date getStartTime() {
    return startTime;
  }

  public void setStartTime( Date startTime ) {
    this.startTime = startTime;
  }

  public Date getEndTime() {
    return endTime;
  }

  public void setEndTime( Date endTime ) {
    this.endTime = endTime;
  }

  public EScheduleType getScheduleType() {
    return scheduleType;
  }

  public void setScheduleType( EScheduleType scheduleType ) {
    this.scheduleType = scheduleType;
  }

  public Boolean getPoweredOn() {
    return poweredOn;
  }

  public void setPoweredOn( Boolean poweredOn ) {
    this.poweredOn = poweredOn;
  }

  public Double getValue() {
    return value;
  }

  public void setValue( Double value ) {
    this.value = value;
  }
}
