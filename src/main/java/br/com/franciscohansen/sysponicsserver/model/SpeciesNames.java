package br.com.franciscohansen.sysponicsserver.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.ManyToAny;
import org.hibernate.annotations.Where;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Where( clause = " ( (deleted is null) or (deleted=false) )" )
public class SpeciesNames extends AbstractModel {

    @ManyToOne
    @JoinColumn(name = "species_id", referencedColumnName = "id")
    @JsonIgnore
    private Species species;
    @Column
    private String locale;
    @Column
    private String commonName;

    @Builder
    public SpeciesNames(String key, Species species, String locale, String commonName) {
        super(key);
        this.species = species;
        this.locale = locale;
        this.commonName = commonName;
    }

    public SpeciesNames() {
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies( Species species ) {
        this.species = species;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale( String locale ) {
        this.locale = locale;
    }

    public String getCommonName() {
        return commonName;
    }

    public void setCommonName( String commonName ) {
        this.commonName = commonName;
    }
}
