package br.com.franciscohansen.sysponicsserver.model.project;

import br.com.franciscohansen.sysponicsserver.model.AbstractModel;
import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.*;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Where( clause = " ( (deleted is null) or (deleted=false) )" )
public class Crop extends AbstractModel {
    @ManyToOne
    @JoinColumn(name = "project_id", referencedColumnName = "id")
    @JsonIgnore
    @JsonDeserialize
    private Project project;

    @Column
    @Temporal(TemporalType.DATE)
    @JsonFormat( pattern = "dd/MM/yyyy", timezone = "America/Sao_Paulo", locale = "pt_BR" )
    private Date plantingDate;
    @Column
    @Temporal(TemporalType.DATE)
    @JsonFormat( pattern = "dd/MM/yyyy", timezone = "America/Sao_Paulo", locale = "pt_BR" )
    private Date expectedHarvestDate;
    @Column
    @Temporal(TemporalType.DATE)
    @JsonFormat( pattern = "dd/MM/yyyy", timezone = "America/Sao_Paulo", locale = "pt_BR" )
    private Date actualHarvestDate;

    private Boolean harvested;

    @OneToOne(mappedBy = "crop", orphanRemoval = true, cascade = CascadeType.ALL)
    private CropSettings cropSettings;

    @OneToMany(cascade = CascadeType.DETACH, orphanRemoval = false, mappedBy = "crop")
    @JsonIgnore
    private List<HistoricalData> historicalData = new ArrayList<>();

    @Lob
    @Type(type = "text")
    @Column
    private String observacoes;

    @Column
    private Long harvestedSpeciesId;

    public Crop() {
        harvested = false;
    }

    @Builder
    public Crop(String key, Date plantingDate, Date expectedHarvestDate, Date actualHarvestDate, Boolean harvested, CropSettings cropSettings, List<HistoricalData> historicalData) {
        super(key);
        this.plantingDate = plantingDate;
        this.expectedHarvestDate = expectedHarvestDate;
        this.actualHarvestDate = actualHarvestDate;
        this.harvested = harvested;
        this.cropSettings = cropSettings;
        this.historicalData = historicalData;
    }


    public void setCropSettings(CropSettings cropSettings) {
        this.cropSettings = cropSettings;
        if (this.cropSettings != null) {
            this.cropSettings.setCrop(this);
        }
    }

    public void addHistoricalData(HistoricalData data) {
        data.setCrop(this);
        this.historicalData.add(data);
    }

    @JsonInclude
    @JsonProperty("settings_id")
    public Long getSettingsId() {
        return cropSettings != null ? cropSettings.getId() : 0L;
    }

    public Project getProject() {
        return project;
    }

    public void setProject( Project project ) {
        this.project = project;
    }

    public Date getPlantingDate() {
        return plantingDate;
    }

    public void setPlantingDate( Date plantingDate ) {
        this.plantingDate = plantingDate;
    }

    public Date getExpectedHarvestDate() {
        return expectedHarvestDate;
    }

    public void setExpectedHarvestDate( Date expectedHarvestDate ) {
        this.expectedHarvestDate = expectedHarvestDate;
    }

    public Date getActualHarvestDate() {
        return actualHarvestDate;
    }

    public void setActualHarvestDate( Date actualHarvestDate ) {
        this.actualHarvestDate = actualHarvestDate;
    }

    public Boolean getHarvested() {
        return harvested;
    }

    public void setHarvested( Boolean harvested ) {
        this.harvested = harvested;
    }

    public CropSettings getCropSettings() {
        return cropSettings;
    }

    public List<HistoricalData> getHistoricalData() {
        return historicalData;
    }

    public void setHistoricalData( List<HistoricalData> historicalData ) {
        this.historicalData = historicalData;
    }

    public String getObservacoes() {
        return observacoes;
    }

    public void setObservacoes( String observacoes ) {
        this.observacoes = observacoes;
    }

    public Long getHarvestedSpeciesId() {
        return harvestedSpeciesId;
    }

    public void setHarvestedSpeciesId( Long harvestedSpeciesId ) {
        this.harvestedSpeciesId = harvestedSpeciesId;
    }
}

