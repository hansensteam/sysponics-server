package br.com.franciscohansen.sysponicsserver.model;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Where( clause = " ( (deleted is null) or (deleted=false) )" )
public class Species extends AbstractModel {

  @Column
  private String cientificName;

  @OneToMany( cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "species" )
  @JsonIgnoreProperties( { "hibernateLazyInitializer", "handler" } )
  private List<SpeciesNames> commonNames = new ArrayList<>();

  public List<SpeciesNames> getCommonNames() {
    if ( this.commonNames == null )
      this.commonNames = new ArrayList<>();
    return commonNames;
  }

  public void setCommonNames( List<SpeciesNames> commonNames ) {
    getCommonNames().clear();
    if ( commonNames != null ) {
      getCommonNames().addAll( commonNames );
    }
  }

  public void addCommonName( SpeciesNames commonName ) {
    commonName.setSpecies( this );
    getCommonNames().add( commonName );
  }

  @Builder
  public Species( String key, String cientificName, List<SpeciesNames> commonNames ) {
    super( key );
    this.cientificName = cientificName;
    this.commonNames = commonNames;
  }

  public Species() {
  }

  public String getCientificName() {
    return cientificName;
  }

  public void setCientificName( String cientificName ) {
    this.cientificName = cientificName;
  }
}
