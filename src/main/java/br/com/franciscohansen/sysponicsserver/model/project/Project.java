package br.com.franciscohansen.sysponicsserver.model.project;

import br.com.franciscohansen.sysponicsserver.model.AbstractModelWithUser;
import br.com.franciscohansen.sysponicsserver.model.Device;
import br.com.franciscohansen.sysponicsserver.model.Species;
import br.com.franciscohansen.sysponicsserver.model.Usuario;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Where( clause = " ( (deleted is null) or (deleted=false) )" )
public class Project extends AbstractModelWithUser {

    private String name;
    @ManyToOne
    @JoinColumn(name = "species_id", referencedColumnName = "id")
    private Species species;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "project_id")
    private List<Crop> crops = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "project")
    private List<Device> devices = new ArrayList<>();

    @Builder
    public Project(String key, Usuario usuario, String name, Species species, List<Crop> crops, List<Device> devices) {
        super(key, usuario);
        this.name = name;
        this.species = species;
        this.crops = crops;
        this.devices = devices;
    }

    public Project() {
    }

    public List<Crop> getCrops() {
        if (this.crops == null) {
            this.crops = new ArrayList<>();
        }
        return crops;
    }

    public void setCrops(List<Crop> crops) {
        getCrops().clear();
        if (crops != null) {
            getCrops().addAll(crops);
        }
    }

    public List<Device> getDevices() {
        if (this.devices == null) {
            this.devices = new ArrayList<>();
        }
        return devices;
    }

    public void setDevices(List<Device> devices) {
        getDevices().clear();
        if (devices != null) {
            getDevices().addAll(devices);
        }
    }

    public void addDevice(Device dev) {
        dev.setProject(this);
        getDevices().add(dev);
    }

    public void addCrop(Crop crop) {
        crop.setProject(this);
        getCrops().add(crop);
    }

    @JsonInclude
    @JsonProperty("species_id")
    public Long getSpeciesId() {
        Long id = 0L;
        if (species != null) {
            id = species.getId();
        }
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public Species getSpecies() {
        return species;
    }

    public void setSpecies( Species species ) {
        this.species = species;
    }
}
