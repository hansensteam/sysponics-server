package br.com.franciscohansen.sysponicsserver.model.project;

import br.com.franciscohansen.sysponicsserver.model.AbstractModel;
import br.com.franciscohansen.sysponicsserver.model.Device;
import br.com.franciscohansen.sysponicsserver.model.enums.EHistoricalDataType;
import com.fasterxml.jackson.annotation.*;
import lombok.*;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import java.util.Date;

@Entity
@Where( clause = " ( (deleted is null) or (deleted=false) )" )
public class HistoricalData extends AbstractModel {

    @ManyToOne
    @JoinColumn(name = "crop_id", referencedColumnName = "id")
    @JsonIgnore
    private Crop crop;

    @ManyToOne(optional = false)
    @JoinColumn(name = "device_id", referencedColumnName = "id")
    @JsonIgnore
    private Device device;

    @Column
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat( pattern = "dd/MM/yyyy HH:mm:ss", timezone = "America/Sao_Paulo", locale = "pt_BR" )
    private Date timestamp;

    private Double value;

    @Enumerated(EnumType.STRING)
    private EHistoricalDataType type;

    @Builder
    public HistoricalData(String key, Crop crop, Device device, Date timestamp, Double value, EHistoricalDataType type) {
        super(key);
        this.crop = crop;
        this.device = device;
        this.timestamp = timestamp;
        this.value = value;
        this.type = type;
    }

    public HistoricalData() {
    }

    @JsonInclude
    @JsonProperty("device_id")
    public Long getDeviceId() {
        return device != null ? device.getId() : 0L;
    }

    @JsonInclude
    @JsonProperty("crop_id")
    public Long getCropId() {
        return crop != null ? crop.getId() : 0L;
    }

    public Crop getCrop() {
        return crop;
    }

    public void setCrop( Crop crop ) {
        this.crop = crop;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice( Device device ) {
        this.device = device;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp( Date timestamp ) {
        this.timestamp = timestamp;
    }

    public Double getValue() {
        return value;
    }

    public void setValue( Double value ) {
        this.value = value;
    }

    public EHistoricalDataType getType() {
        return type;
    }

    public void setType( EHistoricalDataType type ) {
        this.type = type;
    }
}
