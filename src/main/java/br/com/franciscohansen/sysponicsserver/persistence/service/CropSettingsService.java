package br.com.franciscohansen.sysponicsserver.persistence.service;

import br.com.franciscohansen.sysponicsserver.model.project.CropSettings;
import br.com.franciscohansen.sysponicsserver.persistence.repository.CropSettingsRepository;

public interface CropSettingsService extends IService<CropSettings, CropSettingsRepository> {

}
