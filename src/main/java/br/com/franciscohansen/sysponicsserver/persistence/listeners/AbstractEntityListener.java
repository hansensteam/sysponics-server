package br.com.franciscohansen.sysponicsserver.persistence.listeners;


import br.com.franciscohansen.sysponicsserver.components.services.BeanUtil;
import br.com.franciscohansen.sysponicsserver.model.AbstractModel;
import br.com.franciscohansen.sysponicsserver.persistence.repository.IRepository;
import org.apache.commons.lang3.RandomStringUtils;

import javax.persistence.EntityManager;
import javax.persistence.PrePersist;
import java.lang.reflect.ParameterizedType;
import java.util.UUID;

public class AbstractEntityListener {

    @PrePersist
    public void prePersist(AbstractModel obj) {
        if (obj.getKey() == null || obj.getKey().isEmpty()) {
            obj.setKey(
                    UUID.randomUUID()
                            .toString()
                            .replace("-", "")
                            .substring(0, 16)
            );
        }
    }


}
