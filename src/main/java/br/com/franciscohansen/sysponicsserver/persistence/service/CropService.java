package br.com.franciscohansen.sysponicsserver.persistence.service;

import br.com.franciscohansen.sysponicsserver.model.project.Crop;
import br.com.franciscohansen.sysponicsserver.persistence.repository.CropRepository;

public interface CropService extends IService<Crop, CropRepository> {
}
