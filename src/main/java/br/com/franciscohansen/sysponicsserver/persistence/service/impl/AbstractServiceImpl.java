package br.com.franciscohansen.sysponicsserver.persistence.service.impl;

import br.com.franciscohansen.sysponicsserver.model.AbstractModel;
import br.com.franciscohansen.sysponicsserver.persistence.repository.IRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.IService;
import org.apache.commons.lang3.RandomStringUtils;

import javax.transaction.Transactional;
import java.util.Optional;


public abstract class AbstractServiceImpl<T extends AbstractModel, U extends IRepository<T>> implements IService<T, U> {


    protected abstract Class<T> getClassOfT();

    public String getRandomKey() {
        String sKey;
        do {
            sKey = RandomStringUtils.randomAlphanumeric(16);
        } while (getRepository().existsByKey(sKey));
        return sKey;
    }

    protected void doBeforeSave(T t) {

    }

    protected void doAfterSave(T t) {

    }

    @Override
    @Transactional
    public T save(T t) {
        doBeforeSave(t);
        t = getRepository().save(t);
        doAfterSave(t);
        return t;
    }


}
