package br.com.franciscohansen.sysponicsserver.persistence.repository;

import br.com.franciscohansen.sysponicsserver.model.Device;
import br.com.franciscohansen.sysponicsserver.model.project.Project;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
@RepositoryRestResource(path = "projects")
public interface ProjectRepository extends IRepositoryWithUser<Project> {

    List<Project> findAllByDevices( Device dev );

}
