package br.com.franciscohansen.sysponicsserver.persistence.repository;

import br.com.franciscohansen.sysponicsserver.model.project.CropSettings;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "cropSettings", path = "cropSettings")
public interface CropSettingsRepository extends IRepository<CropSettings>{
  CropSettings findFirstByCrop_Id( Long cropId );

}
