package br.com.franciscohansen.sysponicsserver.persistence.repository;

import br.com.franciscohansen.sysponicsserver.model.AbstractModel;
import br.com.franciscohansen.sysponicsserver.model.Usuario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.Optional;

@NoRepositoryBean
public interface IRepository<T extends AbstractModel> extends JpaRepository<T, Long>, JpaSpecificationExecutor<T> {

  boolean existsByKey( String key );

  T findByKey( String key );

}
