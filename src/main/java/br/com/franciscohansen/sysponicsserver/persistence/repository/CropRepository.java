package br.com.franciscohansen.sysponicsserver.persistence.repository;

import br.com.franciscohansen.sysponicsserver.model.project.Crop;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.Optional;

@RepositoryRestResource( path = "crops", collectionResourceRel = "crops" )
public interface CropRepository extends IRepository<Crop> {


  Optional<Crop> findCropByHarvestedFalse();

  Crop findByProjectKeyAndHarvestedFalse( String projectKey );

  List<Crop> findAllByProjectKey( String projectKey, Sort sort );

}
