package br.com.franciscohansen.sysponicsserver.persistence.repository;

import br.com.franciscohansen.sysponicsserver.model.Species;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(path = "/species")
public interface SpeciesRepository extends IRepository<Species> {

}
