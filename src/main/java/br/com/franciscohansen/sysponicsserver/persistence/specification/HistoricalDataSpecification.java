package br.com.franciscohansen.sysponicsserver.persistence.specification;

import br.com.franciscohansen.sysponicsserver.model.enums.EHistoricalDataType;
import br.com.franciscohansen.sysponicsserver.model.project.HistoricalData;
import org.springframework.data.jpa.domain.Specification;

public class HistoricalDataSpecification {

  public static Specification<HistoricalData> whereCropIdAndTypeEquals( long cropId, EHistoricalDataType type ) {
    return ( root, query, builder ) -> builder.and(
            builder.equal( root.get( "crop" ).get( "id" ), cropId ),
            builder.equal( root.get( "type" ), type )
    );
  }

}
