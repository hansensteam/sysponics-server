package br.com.franciscohansen.sysponicsserver.persistence.service.impl;

import br.com.franciscohansen.sysponicsserver.model.Usuario;
import br.com.franciscohansen.sysponicsserver.persistence.repository.UsuarioRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.UsuarioService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UsuarioServiceImpl extends AbstractServiceImpl<Usuario, UsuarioRepository> implements UsuarioService, UserDetailsService {


    private UsuarioRepository usuarioRepository;
    private PasswordEncoder encoder;

    @Autowired
    public UsuarioServiceImpl(UsuarioRepository usuarioRepository, PasswordEncoder encoder) {
        this.usuarioRepository = usuarioRepository;
        this.encoder = encoder;
    }

    @Override
    public UsuarioRepository getRepository() {
        return this.usuarioRepository;
    }

    @Override
    protected Class<Usuario> getClassOfT() {
        return Usuario.class;
    }

    @Override
    public boolean validateLogin(String username, String password) {
        return false;
    }

    @Override
    public Usuario register(Usuario u) {
        if (Optional.ofNullable(u.getId()).orElse(0L).equals(0L)) {
            u.setPassword(encoder.encode(u.getPassword()));
        }
        return save(u);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Optional<Usuario> uOpt = getRepository().findByEmail(s);
        if (uOpt.isPresent()) {
            return uOpt.get();
        } else {
            throw new UsernameNotFoundException(s);
        }
    }
}
