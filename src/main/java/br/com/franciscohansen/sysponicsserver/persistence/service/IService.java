package br.com.franciscohansen.sysponicsserver.persistence.service;

import br.com.franciscohansen.sysponicsserver.model.AbstractModel;
import br.com.franciscohansen.sysponicsserver.persistence.repository.IRepository;

public interface IService<T extends AbstractModel, U extends IRepository<T>> {
    U getRepository();
    T save(T t);
}
