package br.com.franciscohansen.sysponicsserver.persistence.service.impl;

import br.com.franciscohansen.sysponicsserver.components.scheduler.ActuatorScheduler;
import br.com.franciscohansen.sysponicsserver.model.project.CropSchedules;
import br.com.franciscohansen.sysponicsserver.model.project.CropSettings;
import br.com.franciscohansen.sysponicsserver.persistence.repository.CropSettingsRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.CropSettingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CropSettingsServiceImpl extends AbstractServiceImpl<CropSettings, CropSettingsRepository> implements CropSettingsService {

  private CropSettingsRepository repository;
  private ActuatorScheduler scheduler;


  @Autowired
  public CropSettingsServiceImpl( CropSettingsRepository repository, ActuatorScheduler scheduler ) {
    this.repository = repository;
    this.scheduler = scheduler;
  }

  @Override
  protected Class<CropSettings> getClassOfT() {
    return CropSettings.class;
  }

  @Override
  public CropSettingsRepository getRepository() {
    return repository;
  }

  @Override
  protected void doBeforeSave( CropSettings cropSettings ) {
    for ( CropSchedules schedules : cropSettings.getSchedules() ) {
      schedules.setCropSettings( cropSettings );
    }
  }

  @Override
  protected void doAfterSave( CropSettings settings ) {
    try {
      this.scheduler.unscheduleUsuario( settings.getCrop().getProject().getUsuario() );
    }catch ( NullPointerException ignored ){

    }
    this.scheduler.scheduleUsuario( settings.getCrop().getProject().getUsuario() );
  }
}
