package br.com.franciscohansen.sysponicsserver.persistence.service;

import br.com.franciscohansen.sysponicsserver.model.project.Project;
import br.com.franciscohansen.sysponicsserver.persistence.repository.ProjectRepository;

public interface ProjectService extends IService<Project, ProjectRepository> {
}
