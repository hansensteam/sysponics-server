package br.com.franciscohansen.sysponicsserver.persistence.service;

import br.com.franciscohansen.sysponicsserver.model.Species;
import br.com.franciscohansen.sysponicsserver.persistence.repository.SpeciesRepository;

public interface SpeciesService extends IService<Species, SpeciesRepository> {
}
