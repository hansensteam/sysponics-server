package br.com.franciscohansen.sysponicsserver.persistence.repository;

import br.com.franciscohansen.sysponicsserver.model.AbstractModelWithUser;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface IRepositoryWithUser<T extends AbstractModelWithUser> extends IRepository<T> {

}
