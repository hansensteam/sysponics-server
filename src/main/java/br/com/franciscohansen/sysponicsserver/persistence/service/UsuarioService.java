package br.com.franciscohansen.sysponicsserver.persistence.service;

import br.com.franciscohansen.sysponicsserver.model.Usuario;
import br.com.franciscohansen.sysponicsserver.persistence.repository.UsuarioRepository;

public interface UsuarioService extends IService<Usuario, UsuarioRepository> {

    boolean validateLogin(String username, String password);

    Usuario register( Usuario u );

}
