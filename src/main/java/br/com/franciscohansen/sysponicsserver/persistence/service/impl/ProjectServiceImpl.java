package br.com.franciscohansen.sysponicsserver.persistence.service.impl;

import br.com.franciscohansen.sysponicsserver.model.project.Project;
import br.com.franciscohansen.sysponicsserver.persistence.repository.ProjectRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProjectServiceImpl extends AbstractServiceImpl<Project, ProjectRepository> implements ProjectService {

    private ProjectRepository projectRepository;

    @Autowired
    public ProjectServiceImpl(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public ProjectRepository getRepository() {
        return projectRepository;
    }

    @Override
    protected Class<Project> getClassOfT() {
        return Project.class;
    }


    public List<Project> findAllProjectsWithActiveCrops(){


        return null;
    }

}
