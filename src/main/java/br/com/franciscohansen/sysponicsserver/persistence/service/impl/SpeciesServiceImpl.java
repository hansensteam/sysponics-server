package br.com.franciscohansen.sysponicsserver.persistence.service.impl;

import br.com.franciscohansen.sysponicsserver.model.Species;
import br.com.franciscohansen.sysponicsserver.model.SpeciesNames;
import br.com.franciscohansen.sysponicsserver.persistence.repository.SpeciesRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.SpeciesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpeciesServiceImpl extends AbstractServiceImpl<Species, SpeciesRepository> implements SpeciesService {

  private SpeciesRepository speciesRepository;

  @Autowired
  public SpeciesServiceImpl( SpeciesRepository speciesRepository ) {
    this.speciesRepository = speciesRepository;
  }

  @Override
  public SpeciesRepository getRepository() {
    return speciesRepository;
  }

  @Override
  protected Class<Species> getClassOfT() {
    return Species.class;
  }

  @Override
  protected void doBeforeSave( Species species ) {
    super.doBeforeSave( species );
    for ( SpeciesNames sn : species.getCommonNames() ) {
      sn.setSpecies( species );
    }
  }
}
