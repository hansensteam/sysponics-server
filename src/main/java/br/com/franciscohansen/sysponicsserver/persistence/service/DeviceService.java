package br.com.franciscohansen.sysponicsserver.persistence.service;

import br.com.franciscohansen.sysponicsserver.model.Device;
import br.com.franciscohansen.sysponicsserver.persistence.repository.DeviceRepository;

public interface DeviceService extends IService<Device, DeviceRepository> {

}
