package br.com.franciscohansen.sysponicsserver.persistence.repository;

import br.com.franciscohansen.sysponicsserver.model.enums.EScheduleType;
import br.com.franciscohansen.sysponicsserver.model.project.CropSchedules;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource(collectionResourceRel = "cropSchedules", path = "cropSchedules")
public interface CropScheduleRepository extends IRepository<CropSchedules> {

    List<CropSchedules> findAllByScheduleTypeAndCropSettingsId(EScheduleType type, Long id);
}
