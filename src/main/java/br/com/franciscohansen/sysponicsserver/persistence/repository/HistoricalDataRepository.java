package br.com.franciscohansen.sysponicsserver.persistence.repository;

import br.com.franciscohansen.sysponicsserver.model.project.HistoricalData;
import org.springframework.boot.actuate.endpoint.web.annotation.RestControllerEndpoint;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.data.rest.core.annotation.RestResource;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@RepositoryRestResource( path = "historical-data", collectionResourceRel = "historical-data")
public interface HistoricalDataRepository extends IRepository<HistoricalData> {

  Page<HistoricalData> findAllByCrop_Id( @Param( "cropid" ) Long crop, Pageable pageable );

  @RestResource( exported = false )
  List<HistoricalData> findAllByCrop_Id( Long id, Sort sort );

  Page<HistoricalData> findAllByCrop_IdAndTimestampAfter( @Param( "cropid" ) Long cropId,
                                                          @DateTimeFormat( pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS" )
                                                          @Param( "timestamp" ) Date timestamp, Pageable pageable );

}
