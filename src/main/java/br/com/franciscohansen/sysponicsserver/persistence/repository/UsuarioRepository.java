package br.com.franciscohansen.sysponicsserver.persistence.repository;

import br.com.franciscohansen.sysponicsserver.model.Usuario;

import java.util.Optional;

public interface UsuarioRepository extends IRepository<Usuario> {

    Optional<Usuario> findByEmail(String email);

}
