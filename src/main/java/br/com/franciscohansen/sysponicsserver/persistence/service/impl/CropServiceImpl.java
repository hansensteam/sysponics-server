package br.com.franciscohansen.sysponicsserver.persistence.service.impl;

import br.com.franciscohansen.sysponicsserver.components.scheduler.ActuatorScheduler;
import br.com.franciscohansen.sysponicsserver.model.project.Crop;
import br.com.franciscohansen.sysponicsserver.persistence.repository.CropRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.CropService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CropServiceImpl extends AbstractServiceImpl<Crop, CropRepository> implements CropService {

  private CropRepository cropRepository;
  private ActuatorScheduler scheduler;

  @Autowired
  public CropServiceImpl( CropRepository cropRepository, ActuatorScheduler scheduler ) {
    this.cropRepository = cropRepository;
    this.scheduler = scheduler;
  }

  @Override
  public CropRepository getRepository() {
    return cropRepository;
  }

  @Override
  protected Class<Crop> getClassOfT() {
    return Crop.class;
  }

  @Override
  protected void doAfterSave( Crop crop ) {
    super.doAfterSave( crop );
    this.scheduler.unscheduleUsuario( crop.getProject().getUsuario() );
    this.scheduler.scheduleUsuario( crop.getProject().getUsuario() );
  }
}
