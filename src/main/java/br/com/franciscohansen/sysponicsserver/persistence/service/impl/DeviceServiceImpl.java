package br.com.franciscohansen.sysponicsserver.persistence.service.impl;

import br.com.franciscohansen.sysponicsserver.model.Device;
import br.com.franciscohansen.sysponicsserver.persistence.repository.DeviceRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.DeviceService;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DeviceServiceImpl extends AbstractServiceImpl<Device, DeviceRepository> implements DeviceService {

    private DeviceRepository deviceRepository;

    @Autowired
    public DeviceServiceImpl(DeviceRepository deviceRepository) {
        this.deviceRepository = deviceRepository;
    }

    @Override
    public DeviceRepository getRepository() {
        return deviceRepository;
    }

    @Override
    protected Class<Device> getClassOfT() {
        return Device.class;
    }

}
