package br.com.franciscohansen.sysponicsserver.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.ResourceServerTokenServices;

@Configuration
@EnableResourceServer
@EnableGlobalMethodSecurity( prePostEnabled = true, securedEnabled = true )
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {


  @Override
  public void configure( HttpSecurity http ) throws Exception {
    http
            .requestMatchers()
            .and()
            .authorizeRequests()
            .antMatchers( "/actuator/**" ).permitAll()
            .antMatchers( HttpMethod.POST, "/user/register" ).permitAll()
            .anyRequest().authenticated();
//                .antMatchers(HttpMethod.POST, "/user/register").permitAll()
//                .antMatchers("/login/**").permitAll()
//                .antMatchers("/actuator/**", "/api-docs/**", "user/register").permitAll()
//                .antMatchers("/sysponics/**").authenticated();
  }

}
