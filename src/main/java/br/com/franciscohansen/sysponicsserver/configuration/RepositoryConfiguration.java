package br.com.franciscohansen.sysponicsserver.configuration;

import br.com.franciscohansen.sysponicsserver.model.Device;
import br.com.franciscohansen.sysponicsserver.model.Species;
import br.com.franciscohansen.sysponicsserver.model.SpeciesNames;
import br.com.franciscohansen.sysponicsserver.model.Usuario;
import br.com.franciscohansen.sysponicsserver.model.project.*;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurer;

@Configuration
public class RepositoryConfiguration implements RepositoryRestConfigurer {
    @Override
    public void configureRepositoryRestConfiguration(RepositoryRestConfiguration config) {
        config.exposeIdsFor(
                Device.class,
                Species.class,
                SpeciesNames.class,
                Usuario.class,
                Crop.class,
                CropSchedules.class,
                CropSettings.class,
                HistoricalData.class,
                Project.class
        );
    }
}
