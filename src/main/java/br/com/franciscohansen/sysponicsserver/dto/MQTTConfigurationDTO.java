package br.com.franciscohansen.sysponicsserver.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class MQTTConfigurationDTO implements Serializable {

  long millisToVerify;
  long pumpTimeOn;
  long pumpTimeOff;
  long ligthsTimeOn;
  long lightsTimeOff;
  long waterTemp;
  long airTemp;
  long airHum;

}
