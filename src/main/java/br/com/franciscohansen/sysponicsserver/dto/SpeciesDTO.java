package br.com.franciscohansen.sysponicsserver.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class SpeciesDTO implements Serializable {

  private static final long serialVersionUID = 1L;
  private Long id;
  private String cientificName;
  private String commonName;



}
