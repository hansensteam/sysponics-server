package br.com.franciscohansen.sysponicsserver.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.Serializable;

public class MqttDeviceDataDTO implements Serializable {

  @JsonProperty( "user_id" )
  private String userId;
  @JsonProperty( "sensor_id" )
  private String sensorId;
  @JsonProperty( "timestamp" )
  private Long timestamp;
  @JsonProperty( "humidity" )
  private Double humidity;
  @JsonProperty( "temperature" )
  private Double temperature;
  @JsonProperty( "ph" )
  private Double ph;
  @JsonProperty( "low" )
  private int low;

  public String getSensorId() {
    return sensorId;
  }

  public void setSensorId( String sensorId ) {
    this.sensorId = sensorId;
  }

  public Long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp( Long timestamp ) {
    this.timestamp = timestamp;
  }

  public Double getHumidity() {
    return humidity;
  }

  public void setHumidity( Double humidity ) {
    this.humidity = humidity;
  }

  public Double getTemperature() {
    return temperature;
  }

  public void setTemperature( Double temperature ) {
    this.temperature = temperature;
  }

  public Double getPh() {
    return ph;
  }

  public void setPh( Double ph ) {
    this.ph = ph;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId( String userId ) {
    this.userId = userId;
  }

  public int getLow() {
    return low;
  }

  public void setLow( int low ) {
    this.low = low;
  }
}
