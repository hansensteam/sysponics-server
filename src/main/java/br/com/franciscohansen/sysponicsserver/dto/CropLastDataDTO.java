package br.com.franciscohansen.sysponicsserver.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CropLastDataDTO implements Serializable {

  private double airTemp;
  private double airHum;
  private double waterTemp;
  private double waterPh;
  private boolean lightsOn;
  private boolean pumpOn;
  private boolean waterLevelOk;

}
