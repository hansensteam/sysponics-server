package br.com.franciscohansen.sysponicsserver.components.scheduler;

import br.com.franciscohansen.sysponicsserver.components.mqtt.MqttScheduler;
import br.com.franciscohansen.sysponicsserver.model.Device;
import br.com.franciscohansen.sysponicsserver.model.Usuario;
import br.com.franciscohansen.sysponicsserver.model.project.Crop;
import br.com.franciscohansen.sysponicsserver.model.project.CropSchedules;
import br.com.franciscohansen.sysponicsserver.model.project.Project;
import br.com.franciscohansen.sysponicsserver.persistence.repository.CropRepository;
import br.com.franciscohansen.sysponicsserver.persistence.repository.ProjectRepository;
import br.com.franciscohansen.sysponicsserver.persistence.repository.UsuarioRepository;
import lombok.Getter;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import static br.com.franciscohansen.sysponicsserver.util.SchedulerUtils.computeInitialDelay;
import static br.com.franciscohansen.sysponicsserver.util.SchedulerUtils.secondsBetween;

@Component( "actuatorScheduler" )
@DependsOn( { "mqttScheduler" } )
public class ActuatorScheduler {

  private static final Logger LOG = Logger.getLogger( ActuatorScheduler.class.getName() );

  private UsuarioRepository usuarioRepository;
  private CropRepository cropRepository;
  private ProjectRepository projectRepository;
  private MqttScheduler mqttScheduler;

  @Autowired
  private ActuatorScheduler _self;


  @Autowired
  public ActuatorScheduler( UsuarioRepository usuarioRepository, CropRepository cropRepository, ProjectRepository projectRepository, MqttScheduler mqttScheduler ) {
    this.usuarioRepository = usuarioRepository;
    this.cropRepository = cropRepository;
    this.projectRepository = projectRepository;
    this.mqttScheduler = mqttScheduler;
  }

  private static final Map<Long, List<ScheduledFuture>> schedules = new HashMap<>();
  private ScheduledThreadPoolExecutor POOL = new ScheduledThreadPoolExecutor( 10 );


  @Transactional
  @PostConstruct
  public void configureSchedules() {
    LOG.info( "2" );
    List<Usuario> usuarios = usuarioRepository.findAll();
    for ( Usuario usuario : usuarios ) {
      _self.scheduleUsuario( usuario );
    }
  }

  public void unscheduleUsuario( Usuario usuario ) {
    schedules.get( usuario.getId() ).forEach( sf -> sf.cancel( true ) );
    schedules.remove( usuario.getId() );
  }

  @Transactional
  public void scheduleUsuario( Usuario usuario ) {
    List<Project> projects = this.projectRepository.findAll( Specification.where(
            ( root, query, builder ) -> builder.and(
                    builder.isFalse( builder.coalesce( root.get( "deleted" ), false ) ),
                    builder.equal( root.get( "usuario" ).get( "id" ), usuario.getId() )
            )
    ) );
    for ( Project project : projects ) {
      List<Device> devices = project.getDevices();
      Crop crop = this.cropRepository.findByProjectKeyAndHarvestedFalse( project.getKey() );
      List<ScheduledFuture> sch = new ArrayList<>();
      for ( Device device : devices ) {
        switch ( device.getDeviceType() ) {
          case ACT_LIGHTS:
          case ACT_WATER_PUMP: {
            List<CropSchedules> schedules = crop.getCropSettings()
                    .getSchedules()
                    .stream()
                    .filter( sc -> sc.getScheduleType().equals( device.getDeviceType().getScheduleType() ) )
                    .collect( Collectors.toList() );
            sch.addAll( scheduleCrop( usuario, device, schedules ) );
            break;
          }
        }
      }
      if ( !sch.isEmpty() ) {
        schedules.put( usuario.getId(), sch );
      }
    }
  }

  private List<ScheduledFuture> scheduleCrop( Usuario usuario, Device device, List<CropSchedules> schedules ) {
    List<ScheduledFuture> futures = new ArrayList<>();
    for ( CropSchedules sc : schedules ) {
      long start = computeInitialDelay( sc.getStartTime(), sc.getEndTime() );
      if( start == 0L ){
        String topic = "sysponics/user/" + usuario.getKey() + "/config/" + device.getDeviceType().name() + "/return";
        int active = sc.getPoweredOn() ? 1 : 0;
        String payload = "{\"active\":" + active + "}";

        LOG.info( "SENDING " + payload + " TO " + topic );
        mqttScheduler.getCallback().publishMessage( topic, payload );
        start = TimeUnit.DAYS.toSeconds( 1 );
      }
      LOG.info( "Scheduling " + device.getDeviceType().name() + " in " + start + " seconds" );
      long delay = TimeUnit.DAYS.toSeconds( 1 );
      ScheduledFuture future = POOL.scheduleAtFixedRate( new ScheduleRunner( device, usuario, sc, mqttScheduler ), start, delay, TimeUnit.SECONDS );
      futures.add( future );
    }
    return futures;
  }


  @Getter
  private class ScheduleRunner implements Runnable {

    private final Device device;
    private final Usuario usuario;
    private final CropSchedules schedules;
    private final MqttScheduler mqttScheduler;

    private ScheduleRunner( Device device, Usuario usuario, CropSchedules schedules, MqttScheduler mqttScheduler ) {
      this.device = device;
      this.usuario = usuario;
      this.schedules = schedules;
      this.mqttScheduler = mqttScheduler;
    }

    @Override
    public void run() {
      long delay = secondsBetween( schedules.getStartTime(), schedules.getEndTime() );
      String topic = "sysponics/user/" + usuario.getKey() + "/config/" + device.getDeviceType().name() + "/return";
      int active = schedules.getPoweredOn() ? 1 : 0;
      String payload = "{\"active\":" + active + "}";

      LOG.info( "SENDING " + payload + " TO " + topic );
      mqttScheduler.getCallback().publishMessage( topic, payload );
      try {
        Thread.sleep( delay );
      }
      catch ( InterruptedException e ) {
      }
      payload = "{\"active\":" + ( active == 1 ? 0 : 1 ) + "}";
      mqttScheduler.getCallback().publishMessage( topic, payload );
    }
  }


}
