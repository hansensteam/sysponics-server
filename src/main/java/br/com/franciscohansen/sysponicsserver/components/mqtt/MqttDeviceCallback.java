package br.com.franciscohansen.sysponicsserver.components.mqtt;

import br.com.franciscohansen.sysponicsserver.components.scheduler.ActuatorScheduler;
import br.com.franciscohansen.sysponicsserver.dto.MQTTConfigurationDTO;
import br.com.franciscohansen.sysponicsserver.dto.MqttDeviceDataDTO;
import br.com.franciscohansen.sysponicsserver.model.Device;
import br.com.franciscohansen.sysponicsserver.model.Usuario;
import br.com.franciscohansen.sysponicsserver.model.enums.EDeviceType;
import br.com.franciscohansen.sysponicsserver.model.enums.EHistoricalDataType;
import br.com.franciscohansen.sysponicsserver.model.enums.EScheduleType;
import br.com.franciscohansen.sysponicsserver.model.project.Crop;
import br.com.franciscohansen.sysponicsserver.model.project.CropSchedules;
import br.com.franciscohansen.sysponicsserver.model.project.HistoricalData;
import br.com.franciscohansen.sysponicsserver.model.project.Project;
import br.com.franciscohansen.sysponicsserver.persistence.repository.*;
import br.com.franciscohansen.sysponicsserver.util.SchedulerUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.eclipse.paho.client.mqttv3.*;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Component( "mqttDevCallback" )
public class MqttDeviceCallback implements MqttCallback {
  private static final Logger LOG = Logger.getLogger( MqttDeviceCallback.class.getSimpleName() );
  @Value( "${mqtt.host}" )
  private String host;
  @Value( "${mqtt.port}" )
  private int port;
  @Value( "${mqtt.ids.publisher}" )
  private String publisherId;
  @Value( "${mqtt.topics.base}" )
  private String baseTopic;
  @Value( "${mqtt.user}" )
  private String user;
  @Value( "${mqtt.password}" )
  private String password;

  @Autowired
  private MqttDeviceCallback _self;

  private ApplicationContext context;
  private MqttClient client;

  private UsuarioRepository uRepo;
  private DeviceRepository devRepo;
  private ProjectRepository pRepo;
  private CropRepository cRepo;
  private CropScheduleRepository csRepo;
  private ObjectMapper oMapper;
  private HistoricalDataRepository hdRepo;

  @Autowired
  public MqttDeviceCallback( ApplicationContext context, UsuarioRepository uRepo, DeviceRepository devRepo, ProjectRepository pRepo, CropRepository cRepo, CropScheduleRepository csRepo, ObjectMapper oMapper, HistoricalDataRepository hdRepo ) {
    this.context = context;
    this.uRepo = uRepo;
    this.devRepo = devRepo;
    this.pRepo = pRepo;
    this.cRepo = cRepo;
    this.csRepo = csRepo;
    this.oMapper = oMapper;
    this.hdRepo = hdRepo;
  }

  @PostConstruct
  public void dd() {
    LOG.info( "0" );
  }

  @PostConstruct
  public void init() throws MqttException {
    MqttConnectOptions opt = new MqttConnectOptions();
    opt.setCleanSession( true );
    opt.setUserName( user );

    opt.setPassword( password.toCharArray() );
    if( this.client != null ){
      try {
        this.client.disconnectForcibly();
      }catch ( MqttException ignored ){

      }
      this.client = null;
    }

    String publisherId = UUID.randomUUID().toString().replace( "{", "" ).replace( "}", "" );
    LOG.info( publisherId );

    this.client = new MqttClient( String.format( "tcp://%s:%s", host, port ), publisherId, new MemoryPersistence() );
    this.client.setCallback( this );
    this.client.connect( opt );
    while ( !this.client.isConnected() ) {
      //WAIT
    }
    this.client.subscribe( String.format( "%s/+/data", baseTopic ), 1 );
    this.client.subscribe( String.format( "%s/+/config/+", baseTopic ), 1 );
  }

  @Override
  public void connectionLost( Throwable throwable ) {
    try {
      init();
    }
    catch ( MqttException e ) {
      LOG.log( Level.SEVERE, null, e );
    }
  }

  @Override
  public void messageArrived( String s, MqttMessage mqttMessage ) throws Exception {
    String[] topicParts = s.split( "/" );
    String userId = topicParts[ 2 ];
    LOG.info( s );
//    LOG.info( String.format( "[%s] %s", s, new String( mqttMessage.getPayload() ) ) );
    switch ( topicParts[ 3 ] ) {
      case "data": {
        trataDeviceData( new String( mqttMessage.getPayload(), StandardCharsets.UTF_8 ) );
        break;
      }
      case "config": {
        String sType = topicParts[ 4 ];
        if ( !sType.equalsIgnoreCase( "general" ) ) {
          _self.trataConfig( EDeviceType.valueOf( sType ), new String( mqttMessage.getPayload(), StandardCharsets.UTF_8 ), userId );
        }
        else {
          String payload = "{\"millisToVerify\":30000}";
          _self.publishMessage( "sysponics/user/" + userId + "/config/general/return", payload );
//          client.publish( "sysponics/user/" + userId + "/config/general/return",
//                  payload.getBytes( StandardCharsets.UTF_8 ),
//                  1,
//                  false
//          );
        }
        break;
      }
    }
  }

  private void trataDeviceData( String message ) throws IOException {
    LOG.info( message );
    MqttDeviceDataDTO dto = fromJson( message );
    String userKey = dto.getUserId();
    if ( uRepo.existsByKey( userKey ) ) {
      Usuario user = uRepo.findByKey( userKey );
      if ( devRepo.existsByKey( dto.getSensorId() ) ) {
        Device d = devRepo.findByKey( dto.getSensorId() );
        if ( d.getUsuario().getId().equals( user.getId() ) ) {
          _self.addHistoryToProject( d, dto );
        }
      }
    }
  }

  @Transactional
  public void trataConfig( EDeviceType deviceType, String deviceId, String userId ) throws IOException, MqttException {
    List<CropSchedules> sch = new ArrayList<>();
    List<CropSchedules> toSend = new ArrayList<>();
    List<Project> projects = pRepo.findAllByDevices( devRepo.findByKey( deviceId ) );
    MQTTConfigurationDTO.MQTTConfigurationDTOBuilder builder = MQTTConfigurationDTO.builder();
    Long id = 0L;
    for ( Project p : projects ) {
      for ( Crop c : p.getCrops() ) {
        if ( !c.getHarvested() ) {
          sch.addAll( c.getCropSettings().getSchedules() );
          break;
        }
      }
    }
    switch ( deviceType ) {
      case AIR_HUM_SENSOR: {
        for ( CropSchedules cs : sch ) {
          if ( cs.getScheduleType().equals( EScheduleType.AIR_HUM ) ) {
            toSend.add( cs );
          }
        }
      }
      case AIR_TEMP_SENSOR: {
        for ( CropSchedules cs : sch ) {
          if ( cs.getScheduleType().equals( EScheduleType.AIR_TEMP ) ) {
            toSend.add( cs );
          }
        }
        break;
      }
      case WATER_TEMP_SENSOR: {
        for ( CropSchedules cs : sch ) {
          if ( cs.getScheduleType().equals( EScheduleType.WATER_TEMP ) ) {
            toSend.add( cs );
          }
        }
        break;
      }
      case AIR_TEMP_HUM_SENSOR: {
        for ( CropSchedules cs : sch ) {
          if ( cs.getScheduleType().equals( EScheduleType.AIR_HUM ) ||
                  cs.getScheduleType().equals( EScheduleType.AIR_TEMP ) ) {
            toSend.add( cs );
          }
        }
        break;
      }
      case ACT_AIR_CHILLER:
      case ACT_AIR_HEATER:
      case ACT_AIR_PUMP:
      case ACT_FAN:
      case ACT_LIGHTS:
      case ACT_SPRINKLER:
      case ACT_WATER_PUMP:
      case ACT_WATER_CHILLER:
      case ACT_WATER_HEATER: {
        this.context.getBean( ActuatorScheduler.class ).scheduleUsuario( uRepo.findByKey( userId ) );
        this.context.getBean( ActuatorScheduler.class ).unscheduleUsuario( uRepo.findByKey( userId ) );
        return;
      }
//      case ACT_LIGHTS: {
//        for ( CropSchedules cs : sch ) {
//          if ( cs.getScheduleType().equals( EScheduleType.LIGHTS ) ) {
////            toSend.add( cs );
//            ZoneId zone = ZoneId.of( "America/Sao_Paulo" );
//            long offset = TimeZone.getTimeZone( zone ).getRawOffset();
//            long start = cs.getStartTime().getTime() + offset;
//            long end = cs.getEndTime().getTime() + offset;
//            builder.ligthsTimeOn( start )
//                    .lightsTimeOff( end );
//          }
//        }
//        break;
//      }
    }

    try {
      ByteArrayOutputStream out = new ByteArrayOutputStream();
      oMapper.writeValue( out, builder.build() );
      String topic = String.format( "%s/%s/config/%s/return", baseTopic, userId, deviceId );
      String payload = new String( out.toByteArray(), StandardCharsets.UTF_8 );
      LOG.info( topic );
      LOG.info( payload );
      publishMessage( topic, payload );
//      client.publish( topic,
//              payload.getBytes( StandardCharsets.UTF_8 ),
//              1,
//              false
//      );
    }
    catch ( Exception e ) {
      LOG.log( Level.SEVERE, "MqttDeviceCallback::trataConfig", e );
    }
  }

  public void publishMessage( String topic, String payload ) {
    try {
//      if ( !this.client.isConnected() ) {
//        this.init();
//      }
      this.client.publish( topic, payload.getBytes( StandardCharsets.UTF_8 ), 1, false );
    }
    catch ( MqttException e ) {
      LOG.log( Level.SEVERE, null, e );
    }
  }


  @Override
  public void deliveryComplete( IMqttDeliveryToken iMqttDeliveryToken ) {

  }

  @Transactional
  public void addHistoryToProject( Device dev, MqttDeviceDataDTO dto ) {
    List<HistoricalData> data = new ArrayList<>();
    Date timestamp = Date.from( LocalDateTime.now().atZone( ZoneId.systemDefault() ).toInstant() );
    switch ( dev.getDeviceType() ) {
      case WATER_TEMP_SENSOR: {
        HistoricalData hd = new HistoricalData();
        hd.setTimestamp( timestamp );
        hd.setDevice( dev );
        hd.setValue( dto.getTemperature() );
        hd.setType( EHistoricalDataType.WATER_TEMP );
        data.add( hd );
        break;
      }
      case WATER_PH_SENSOR: {
        HistoricalData hd = new HistoricalData();
        hd.setDevice( dev );
        hd.setTimestamp( timestamp );
        hd.setValue( dto.getPh() );
        hd.setType( EHistoricalDataType.WATER_PH );
        data.add( hd );
        break;
      }
      case AIR_TEMP_SENSOR: {
        HistoricalData hd = new HistoricalData();
        hd.setDevice( dev );
        hd.setTimestamp( timestamp );
        hd.setValue( dto.getTemperature() );
        hd.setType( EHistoricalDataType.AIR_TEMP );
        data.add( hd );
        break;
      }
      case AIR_HUM_SENSOR: {
        HistoricalData hd = new HistoricalData();
        hd.setDevice( dev );
        hd.setTimestamp( timestamp );
        hd.setValue( dto.getHumidity() );
        hd.setType( EHistoricalDataType.AIR_HUM );
        data.add( hd );
        break;
      }
      case AIR_TEMP_HUM_SENSOR: {
        HistoricalData hd1 = new HistoricalData();
        hd1.setDevice( dev );
        hd1.setTimestamp( timestamp );
        hd1.setValue( dto.getTemperature() );
        hd1.setType( EHistoricalDataType.AIR_TEMP );
        data.add( hd1 );
        HistoricalData hd2 = new HistoricalData();
        hd2.setTimestamp( timestamp );
        hd2.setDevice( dev );
        hd2.setValue( dto.getHumidity() );
        hd2.setType( EHistoricalDataType.AIR_HUM );
        data.add( hd2 );
        break;
      }
      case ACT_LIGHTS:
      case ACT_WATER_PUMP:
      case ACT_AIR_PUMP:
      case ACT_FAN:
      case ACT_WATER_CHILLER:
      case ACT_AIR_CHILLER:
      case ACT_WATER_HEATER:
      case ACT_AIR_HEATER:
      case ACT_SPRINKLER:
      case WATER_LEVEL_SENSOR: {
        HistoricalData hd = new HistoricalData();
        hd.setDevice( dev );
        hd.setTimestamp( timestamp );
        hd.setValue( Double.valueOf( String.valueOf( dto.getLow() ) ) );
        hd.setType( dev.getDeviceType().getHDType() );
        data.add( hd );
        break;
      }

    }
    List<Project> projs = pRepo.findAllByDevices( dev );
    List<Crop> toUpdate = new ArrayList();
    for ( Project p : projs ) {
      for ( Crop c : p.getCrops() ) {
        if ( !c.getHarvested() ) {
          for ( HistoricalData d : data ) {
            checkDataThreshold( p, c, d );
            d.setCrop( c );
          }
          toUpdate.add( c );
        }
      }
    }
    this.hdRepo.saveAll( data );
  }

  private List<CropSchedules> currentSchedule( List<CropSchedules> list ) {
    List<CropSchedules> l = new ArrayList<>();
    for ( CropSchedules sc : list ) {
      LocalDateTime start = LocalDateTime.of( LocalDate.now(), SchedulerUtils.localTimeOfTime( sc.getStartTime(), true ) );
      LocalDateTime end = LocalDateTime.of( LocalDate.now(), SchedulerUtils.localTimeOfTime( sc.getEndTime(), false ) );
      if ( start.isAfter( end ) ) {
        end = end.plusDays( 1 );
      }
      if ( LocalDateTime.now().isAfter( start ) && LocalDateTime.now().isBefore( end ) ) {
        l.add( sc );
      }
    }
    return l;
  }


  private void checkDataThreshold( Project project, Crop crop, HistoricalData data ) {
    List<CropSchedules> schedules = crop.getCropSettings().getSchedules();
    try {
      switch ( data.getType() ) {
        case WATER_TEMP: {
          List<CropSchedules> temp = schedules.stream()
                  .filter( sc -> sc.getScheduleType().equals( EScheduleType.WATER_TEMP ) )
                  .sorted( Comparator.comparing( CropSchedules::getId ).reversed() )
                  .collect( Collectors.toList() );
          temp = currentSchedule( temp );
          if ( !temp.isEmpty() ) {
            CropSchedules schedule = temp.get( 0 );
            if ( data.getValue() < schedule.getValue() - 1 ) {
              turnOnOff( project.getUsuario(), EDeviceType.ACT_WATER_CHILLER, false );
              turnOnOff( project.getUsuario(), EDeviceType.ACT_WATER_HEATER, true );
            }
            else if ( data.getValue() > schedule.getValue() + 1 ) {
              turnOnOff( project.getUsuario(), EDeviceType.ACT_WATER_CHILLER, true );
              turnOnOff( project.getUsuario(), EDeviceType.ACT_WATER_HEATER, false );
            }
            else {
              turnOnOff( project.getUsuario(), EDeviceType.ACT_WATER_CHILLER, false );
              turnOnOff( project.getUsuario(), EDeviceType.ACT_WATER_HEATER, false );
            }
          }
          break;
        }
        case AIR_TEMP: {
          List<CropSchedules> temp = schedules.stream().filter( sc -> sc.getScheduleType().equals( EScheduleType.AIR_TEMP ) )
                  .sorted( Comparator.comparing( CropSchedules::getId ).reversed() )
                  .collect( Collectors.toList() );
          temp = currentSchedule( temp );
          if ( !temp.isEmpty() ) {
            CropSchedules schedule = temp.get( 0 );
            if ( data.getValue() < schedule.getValue() - 1 ) {
              turnOnOff( project.getUsuario(), EDeviceType.ACT_AIR_CHILLER, false );
              turnOnOff( project.getUsuario(), EDeviceType.ACT_AIR_HEATER, true );
            }
            else if ( data.getValue() > schedule.getValue() + 1 ) {
              turnOnOff( project.getUsuario(), EDeviceType.ACT_AIR_CHILLER, true );
              turnOnOff( project.getUsuario(), EDeviceType.ACT_AIR_HEATER, false );
            }
            else {
              turnOnOff( project.getUsuario(), EDeviceType.ACT_AIR_CHILLER, false );
              turnOnOff( project.getUsuario(), EDeviceType.ACT_AIR_HEATER, false );
            }
          }
          break;
        }
        case AIR_HUM: {
          List<CropSchedules> temp = schedules.stream().filter( sc -> sc.getScheduleType().equals( EScheduleType.AIR_TEMP ) )
                  .sorted( Comparator.comparing( CropSchedules::getId ).reversed() )
                  .collect( Collectors.toList() );
          temp = currentSchedule( temp );
          if ( !temp.isEmpty() ) {
            CropSchedules schedule = temp.get( 0 );
            if ( data.getValue() < schedule.getValue() - 3 ) {
              turnOnOff( project.getUsuario(), EDeviceType.ACT_SPRINKLER, false );
              turnOnOff( project.getUsuario(), EDeviceType.ACT_AIR_HEATER, true );
            }
            else if ( data.getValue() > schedule.getValue() + 3 ) {
              turnOnOff( project.getUsuario(), EDeviceType.ACT_SPRINKLER, true );
              turnOnOff( project.getUsuario(), EDeviceType.ACT_AIR_HEATER, false );
            }
            else {
              turnOnOff( project.getUsuario(), EDeviceType.ACT_SPRINKLER, false );
              turnOnOff( project.getUsuario(), EDeviceType.ACT_AIR_HEATER, false );
            }
          }
          break;
        }
      }
    }
    catch ( MqttException e ) {
      LOG.log( Level.SEVERE, null, e );
      e.printStackTrace();
    }
  }

  private void turnOnOff( Usuario usuario, EDeviceType type, boolean on ) throws MqttException {
    String payload = "{\"active\":" + ( on ? "1" : "0" ) + "}";
    String topic = "sysponics/user/" + usuario.getKey() + "/config/" + type.name() + "/return";
    _self.publishMessage( topic, payload );
//    this.client.publish( topic, payload.getBytes( StandardCharsets.UTF_8 ), 1, false );
  }


  private MqttDeviceDataDTO fromJson( String msg ) throws IOException {
    return oMapper.readValue( msg, MqttDeviceDataDTO.class );
  }

  public MqttClient getClient() {
    return client;
  }
}
