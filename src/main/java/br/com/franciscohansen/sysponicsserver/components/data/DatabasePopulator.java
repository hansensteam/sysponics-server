package br.com.franciscohansen.sysponicsserver.components.data;

import br.com.franciscohansen.sysponicsserver.model.Device;
import br.com.franciscohansen.sysponicsserver.model.Species;
import br.com.franciscohansen.sysponicsserver.model.SpeciesNames;
import br.com.franciscohansen.sysponicsserver.model.Usuario;
import br.com.franciscohansen.sysponicsserver.model.enums.EDeviceType;
import br.com.franciscohansen.sysponicsserver.model.enums.EScheduleType;
import br.com.franciscohansen.sysponicsserver.model.project.Crop;
import br.com.franciscohansen.sysponicsserver.model.project.CropSchedules;
import br.com.franciscohansen.sysponicsserver.model.project.CropSettings;
import br.com.franciscohansen.sysponicsserver.model.project.Project;
import br.com.franciscohansen.sysponicsserver.persistence.repository.UsuarioRepository;
import br.com.franciscohansen.sysponicsserver.persistence.service.*;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.*;

@Component
public class DatabasePopulator {
    @Autowired
    private DatabasePopulator _self;
    private SpeciesService speciesService;
    private UsuarioRepository usuarioService;
    private DeviceService deviceService;
    private ProjectService projectService;
    private CropService cropService;
    private PasswordEncoder encoder;

    @Autowired
    public DatabasePopulator(SpeciesService speciesService,
                             UsuarioRepository usuarioService,
                             DeviceService deviceService,
                             ProjectService projectService,
                             CropService cropService) {
        this.speciesService = speciesService;
        this.usuarioService = usuarioService;
        this.deviceService = deviceService;
        this.projectService = projectService;
        this.cropService = cropService;
        this.encoder = new BCryptPasswordEncoder();
    }

    @PostConstruct
    public void init() {

    }

    @Transactional
    public void registerDevice() {
        Device d = new Device();
        d.setDescription("Sensor de Temperatura e Humidade");
        d.setName("DHT22");
        d.setDeviceType(EDeviceType.AIR_TEMP_HUM_SENSOR);
        d.setKey("rszkhajoldyccmyw");
        d.setUsuario(usuarioService.getOne(1L));
        deviceService.save(d);
    }

    @Transactional
    public void registerDefaultUser() {
        Usuario u = new Usuario();
        u.setNome("Francisco Hansen");
        u.setEmail("francisco.hansen@gmail.com");
        u.setPassword(encoder.encode("123456"));
        u.setKey("wdltdfrmbgmhnfaa");
        usuarioService.save(u);
    }

    @Transactional
    public void registerSpecies() {
        Species sp = new Species();
        sp.setCientificName("Fragaria");

        SpeciesNames spn1 = new SpeciesNames();
        spn1.setCommonName("Morango");
        spn1.setLocale("pt_br");
        sp.addCommonName(spn1);

        SpeciesNames spn2 = new SpeciesNames();
        spn2.setCommonName("Strawberry");
        spn2.setLocale("en");
        sp.addCommonName(spn2);
        speciesService.save(sp);
    }

    @Transactional
    public void registerCrop() {
        Project p = projectService.getRepository().findAll().get(0);
        Crop crop = new Crop();
        crop.setPlantingDate(Calendar.getInstance().getTime());
        CropSettings sets = new CropSettings();
        sets.setWaterPH(7.5);
        LocalDateTime start = LocalDateTime.of(LocalDate.now(), LocalTime.of(7, 0, 0, 0));
        LocalDateTime end = LocalDateTime.of(LocalDate.now(), LocalTime.of(18, 0, 0, 0));

        CropSchedules schedules = CropSchedules.builder()
                .scheduleType(EScheduleType.LIGHTS)
                .startTime(Date.from(start.atZone(ZoneId.systemDefault()).toInstant()))
                .endTime(Date.from(end.atZone(ZoneId.systemDefault()).toInstant()))
                .poweredOn(false)
                .build();
        sets.addSchedule(schedules);
        CropSchedules schedules2 = CropSchedules.builder()
                .scheduleType(EScheduleType.AIR_TEMP)
                .startTime(Date.from(start.atZone(ZoneId.systemDefault()).toInstant()))
                .endTime(Date.from(end.atZone(ZoneId.systemDefault()).toInstant()))
                .poweredOn(false)
                .value(25D)
                .build();
        sets.addSchedule(schedules2);
        CropSchedules schedules3 = CropSchedules.builder()
                .scheduleType(EScheduleType.AIR_HUM)
                .startTime(Date.from(start.atZone(ZoneId.systemDefault()).toInstant()))
                .endTime(Date.from(end.atZone(ZoneId.systemDefault()).toInstant()))
                .poweredOn(false)
                .value(50D)
                .build();
        sets.addSchedule(schedules3);

        crop.setCropSettings(sets);
        crop.setProject(p);

        p.addCrop(crop);
        projectService.save(p);
    }

    @Transactional
    public void registerProject() {
        Project proj = new Project();
        Optional<Species> sp = speciesService.getRepository().findById(1L);
        Species species = sp.get();
        Hibernate.initialize(species.getCommonNames());
        proj.setSpecies(species);
        proj.setName("Projeto Teste");
        proj.setUsuario(usuarioService.findByEmail("francisco.hansen@gmail.com").get());
        proj = projectService.save(proj);

        Device dev = deviceService.getRepository().findAll().get(0);
        proj.addDevice(dev);
        projectService.save(proj);
    }
}
