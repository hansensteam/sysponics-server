package br.com.franciscohansen.sysponicsserver.components.mqtt;

import org.eclipse.paho.client.mqttv3.MqttException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

@Component("mqttScheduler")
@DependsOn( { "mqttDevCallback" } )
public class MqttScheduler {

    private static final Logger LOG = Logger.getLogger(MqttScheduler.class.getName());
    private final ScheduledThreadPoolExecutor _executor = new ScheduledThreadPoolExecutor(10);

    //    private MQTTClientConfig config;
    private MqttDeviceCallback callback;

    @Autowired
    public MqttScheduler(MqttDeviceCallback callback) {
        this.callback = callback;
    }

    @PostConstruct
    public void init() {
        LOG.info( "1" );
        _executor.scheduleAtFixedRate(() -> {
            try {
                if (callback.getClient() == null ||
                        !callback.getClient().isConnected()) {
                    callback.init();
                }
            } catch (MqttException e) {
                LOG.log(Level.SEVERE, null, e.getMessage());
            }
        }, 0L, 10L, TimeUnit.SECONDS);

    }

    public MqttDeviceCallback getCallback() {
        return callback;
    }

    public void setCallback( MqttDeviceCallback callback ) {
        this.callback = callback;
    }
}
